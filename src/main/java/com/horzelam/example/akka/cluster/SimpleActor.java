package com.horzelam.example.akka.cluster;

import akka.actor.AbstractLoggingActor;
import akka.actor.ActorSelection;
import akka.actor.Props;
import akka.cluster.Cluster;
import akka.cluster.ClusterEvent;
import akka.cluster.Member;
import akka.japi.pf.ReceiveBuilder;
import scala.concurrent.duration.FiniteDuration;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class SimpleActor extends AbstractLoggingActor {
    public static final String SIMPLEACTOR_NAME = "simpleActor";

    private int minNodesForQuorum;

    private Cluster cluster = Cluster.get(getContext().getSystem());

    private Map<String, Member> otherMembers = new HashMap<>();

    public SimpleActor(final int minNodesForQuorum) {

        this.minNodesForQuorum = minNodesForQuorum;
    }

    private final Receive ready = ReceiveBuilder.create()
            // -- handle Text msg
            .match(String.class, msg -> handleTextMsg(msg))
            // -- handle all Cluster messages
            .match(ClusterEvent.MemberUp.class, mUp -> {

                final Member member = mUp.member();
                if(!member.address().toString().equals(cluster.selfMember().address().toString())) {
                    //info only about our own node:
                    log().info("Other node in the cluster: {}", member.address());
                    otherMembers.put(member.address().toString(), member);


                    if(minNodesForQuorum <= otherMembers.size() + 1) {
                        otherMembers.forEach((address, registeredMember)  -> {
                            final ActorSelection otherSimpleActor = getContext().actorSelection(address + "/user/" + self().path().name());
                            otherSimpleActor.tell(new String("Cluster QUORUM achieved"), getSelf());
                        });
                    }
                }

            }).match(ClusterEvent.MemberRemoved.class, mRemoved -> {
                final Member removed = mRemoved.member();
                log().info("Member is Removed: {}", removed);
                otherMembers.remove(removed.address().toString());
                if(minNodesForQuorum > otherMembers.size() + 1) {
                    log().info("no more QUORUM");
                }
            }).match(ClusterEvent.UnreachableMember.class, mUnreachable -> {
                log().warning("Member detected as unreachable: {}", mUnreachable.member());
            }).match(ClusterEvent.MemberEvent.class, message -> {
                // ignore
            }).build();

    @Override
    public Receive createReceive() {
        return ready;
    }

    private void handleTextMsg(final String msg) {
        log().info("Received : '{}' from '{}'", msg, sender().path());
    }

    public static Props props(final int minNodesForQuorum) {
        return Props.create(SimpleActor.class, () -> new SimpleActor(minNodesForQuorum));
    }

    @Override
    public void preStart() throws Exception {
        cluster.subscribe(getSelf(), ClusterEvent.initialStateAsEvents(), ClusterEvent.MemberEvent.class,
                ClusterEvent.UnreachableMember.class);
    }

    //re-subscribe when restart
    @Override
    public void postStop() {
        cluster.unsubscribe(getSelf());
    }
}
