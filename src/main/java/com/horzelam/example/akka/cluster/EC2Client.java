package com.horzelam.example.akka.cluster;

import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.AmazonEC2ClientBuilder;
import com.amazonaws.services.ec2.model.DescribeInstancesRequest;
import com.amazonaws.services.ec2.model.DescribeInstancesResult;
import com.amazonaws.services.ec2.model.Instance;
import com.amazonaws.services.ec2.model.InstanceStateName;
import com.amazonaws.services.ecs.AmazonECS;
import com.amazonaws.services.ecs.AmazonECSClientBuilder;
import com.amazonaws.services.ecs.model.ContainerInstance;
import com.amazonaws.services.ecs.model.DescribeContainerInstancesRequest;
import com.amazonaws.services.ecs.model.DescribeContainerInstancesResult;
import com.amazonaws.services.ecs.model.ListContainerInstancesRequest;
import com.amazonaws.services.ecs.model.ListContainerInstancesResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

public class EC2Client {

    private static final Logger LOGGER = LoggerFactory.getLogger(EC2Client.class);
    private static final String AWS_META_INFO_ENDPOINT = "http://169.254.169.254/latest";

    private final AmazonEC2 ec2;
    private final AmazonECS ecs;

    public EC2Client() {
        // Note: currently the application when run on cluster nodes uses account configured in node: ~/.aws/credentials
        // This account is defined to access AWS API.
        // Without it - default container account was used :
        // .... arn:aws:sts::109661562808:assumed-role/ecsInstanceRole/i-0245a4eb23ec81c4a is not authorized to perform: ecs:ListContainerInstances on resource: arn:aws:ecs:eu-west-1:109661562808:cluster/test-akka-cluster-v2
        // Proper application should run on dedicated account which has preper role with rights allowing AWS API access.
        ec2 = AmazonEC2ClientBuilder.defaultClient();
        ecs = AmazonECSClientBuilder.defaultClient();
    }

    public static class ClusterInfo {
        public String hostIP;
        public List<String> clusterNodes;

        public ClusterInfo(final String hostIP, final List<String> clusterNodes) {
            this.hostIP = hostIP;
            this.clusterNodes = clusterNodes;
        }
    }

    public ClusterInfo getClusterInfo(final String cluster) {

        final Map<String, String> ecInstancesIPs = new HashMap<>();

        final ListContainerInstancesResult instances = ecs.listContainerInstances(new ListContainerInstancesRequest().withCluster(cluster));
        for(final String instanceArn : instances.getContainerInstanceArns()) {
            LOGGER.trace("Found cluster container" + instanceArn);
            final DescribeContainerInstancesResult containerDesciption = ecs.describeContainerInstances(
                    new DescribeContainerInstancesRequest().withCluster(cluster).withContainerInstances(instanceArn));

            final List<ContainerInstance> listOfContainerInstances = containerDesciption.getContainerInstances();
            listOfContainerInstances.forEach(item -> {
                final DescribeInstancesResult instanceInfo =
                        ec2.describeInstances(new DescribeInstancesRequest().withInstanceIds(item.getEc2InstanceId()));
                final String instanceId = instanceInfo.getReservations().get(0).getInstances().get(0).getInstanceId();
                LOGGER.trace("EC2 instance: " + instanceId);
                final Optional<Instance> runningInstance = instanceInfo.getReservations()
                        .get(0)
                        .getInstances()
                        .stream()
                        .filter(instance -> instance.getState().getName().equals(InstanceStateName.Running.toString()))
                        .findFirst();
                runningInstance.ifPresent(instance -> {
                    LOGGER.info("Found EC2 instance in cluster with private IP address: " + instance.getPrivateIpAddress());
                    ecInstancesIPs.put(instanceId, instance.getPrivateIpAddress());
                });
            });
        }
        if(ecInstancesIPs.isEmpty()) {
            throw new IllegalStateException("Unable to determine any IP address");
        }

        final String currentInstanceId = getCurrentIstanceId();
        LOGGER.info("Current Instance ID: " + currentInstanceId);

        if(!ecInstancesIPs.containsKey(currentInstanceId)) {
            throw new IllegalStateException("Unable to find currentInstanceId among instances listed for cluster: " + cluster);
        }

        final String hostIP = ecInstancesIPs.get(currentInstanceId);

        final List<String> allIPs = ecInstancesIPs.entrySet().stream().map(entry -> entry.getValue()).collect(Collectors.toList());
        return new ClusterInfo(hostIP, allIPs);

    }

    private String getCurrentIstanceId() {
        try {
            // discovering IP address using AWS meta info:
            final URLConnection conn = new URL(AWS_META_INFO_ENDPOINT + "/meta-data/instance-id").openConnection();
            try(final BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()))) {
                return in.readLine();
            }
        } catch(IOException e) {
            throw new RuntimeException("Unable to get currentInstance ID", e);
        }
    }
}
