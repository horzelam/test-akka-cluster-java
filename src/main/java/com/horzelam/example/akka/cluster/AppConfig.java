package com.horzelam.example.akka.cluster;

import java.util.List;

public class AppConfig {

    private final String host;
    private final int port;
    private final List<String> otherHosts;
    private final int minNodesForQuorum;

    public AppConfig(final String host, final int port, final List<String> otherHosts, final int minNodesForQuorum) {
        this.host = host;
        this.port = port;
        this.otherHosts = otherHosts;
        this.minNodesForQuorum = minNodesForQuorum;
    }

    public String getHost() {
        return host;
    }

    public int getPort() {
        return port;
    }

    public List<String> getOtherHosts() {
        return otherHosts;
    }

    public int getMinNodesForQuorum() {
        return minNodesForQuorum;
    }

}
