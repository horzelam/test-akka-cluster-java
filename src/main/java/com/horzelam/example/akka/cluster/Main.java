package com.horzelam.example.akka.cluster;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import com.amazonaws.util.StringUtils;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import com.typesafe.config.ConfigValueFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static com.horzelam.example.akka.cluster.SimpleActor.SIMPLEACTOR_NAME;

public class Main {

    private static final Logger LOGGER = LoggerFactory.getLogger(Main.class);
    private static final String SYSTEM_NAME = "actorSystemName";
    // default port when running in cluster , also default seed port when running locally on single node
    private static final int DEFAULT_PORT = 2551;

    private ActorSystem actorSystem;

    public static void main(String[] args) {
        System.out.println("Starting main...");
        final Main main = new Main();
        main.start();
    }

    private void start() {

        // --- Main params ---
        final AppConfig appConfig = prepareAppConfig();

        // set seeds - based on auto-discovered IPs (or local one) but always with default port (2551)
        final List<String> seedNodes = ipsToAkkaSeedsPaths(appConfig.getOtherHosts());
        //Arrays.asList("akka.tcp://actorSystemName@127.0.0.1:2551","akka.tcp://actorSystemName@127.0.0.1:2552");
        LOGGER.info("Using other hosts akka paths: " + seedNodes);

        // default config from file:
        final Config defaultConfig = ConfigFactory.load();

        final Config config = ConfigFactory.empty()
                .withValue("akka.cluster.seed-nodes", ConfigValueFactory.fromIterable(seedNodes))
                .withValue("akka.remote.netty.tcp.hostname", ConfigValueFactory.fromAnyRef(appConfig.getHost()))
                .withValue("akka.remote.netty.tcp.port", ConfigValueFactory.fromAnyRef(appConfig.getPort()));

        LOGGER.info("Starting ActorSystem...");
        actorSystem = ActorSystem.create(SYSTEM_NAME, config.withFallback(defaultConfig));

        // creating 1 SimpleActor per node:
        final ActorRef simpleActor = actorSystem.actorOf(SimpleActor.props(appConfig.getMinNodesForQuorum()), SIMPLEACTOR_NAME);
        simpleActor.tell(new String("initial message"), ActorRef.noSender());

        LOGGER.info("App started successfully");

    }

    private List<String> ipsToAkkaSeedsPaths(final List<String> otherHosts) {
        return otherHosts.stream().map(host -> "akka.tcp://" + SYSTEM_NAME + "@" + host + ":" + DEFAULT_PORT).collect(Collectors.toList());
    }

    private AppConfig prepareAppConfig() {
        // check system properties:
        final String clusterParam = System.getProperty("cluster"); // e.g.: "test-akka-cluster-v2";
        final String portParam = System.getProperty("akka.port");
        if(StringUtils.isNullOrEmpty(clusterParam) && StringUtils.isNullOrEmpty(portParam)) {
            throw new IllegalArgumentException("One (and only one) of param must be set: cluster | akka.port ");
        }
        final String minNodesForQuorumParam = System.getProperty("minNodesForQuorumParam");
        int minNodesForQuorum = 3;
        if(StringUtils.isNullOrEmpty(minNodesForQuorumParam) && StringUtils.isNullOrEmpty(minNodesForQuorumParam)) {
            LOGGER.info("Assuming min amount of nodes for quorum: {}", minNodesForQuorum);
        } else {
            minNodesForQuorum = Integer.parseInt(minNodesForQuorumParam);
        }

        // obtain host/otherHosts depending on mode: local/cluster
        final boolean runningOnAwsCluster = !StringUtils.isNullOrEmpty(clusterParam);

        if(runningOnAwsCluster) {

            LOGGER.info("Running on ECS cluster ");
            final EC2Client.ClusterInfo clusterInfo = new EC2Client().getClusterInfo(clusterParam);

            if(clusterInfo.clusterNodes.isEmpty()) {
                LOGGER.warn("It's not possible to discover other AKKA nodes because there is no other cluster node");
            } else {
                LOGGER.info("IPs of cluster nodes to check : {}", clusterInfo.clusterNodes);
            }

            return new AppConfig(clusterInfo.hostIP, DEFAULT_PORT, clusterInfo.clusterNodes, minNodesForQuorum);
        } else {
            final int port = Integer.parseInt(portParam);
            LOGGER.info("Running locally on port: {}", port);
            return new AppConfig("127.0.0.1", port, Arrays.asList("127.0.0.1"), minNodesForQuorum);
        }
    }
}
