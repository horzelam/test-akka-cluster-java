# AKKA CLUSTER on AWS Cluster with auto-discovery
Example simple Akka cluster application which automatically discovers other nodes
in the same AWS Cluster.

Initial params are :   
* cluster name (however it could be discovered as well)  
* min amount of nodes to form QUORUM (just to show that nodes communicate with each other)  

The app can be tested locally (without auto-discovery) on different ports

# How to build
```
mvn clean package
```
# How to prepare env
Prepare AWS ECS cluster with 2 or more nodes
Each node :  
* security group allowing TCP communication on ports 2551..2555  
* ssh access  
* jre 8 installed   
* has rights to use AWS API to get meta info about cluster nodes (ecs, ec2 API)  


Regarding rights to access AWS API (ECS/EC2) I simply
prepared .aws config with credentials and put it into each cluster node.
This solution was just quick workaround but better would be assign proper role to cluster node/container.

When you use Amazon auto-scalable groups then the discovery must be changed - using another Amazon API - Amazon EC2 Auto Scaling. 

# How to deploy manually
```
cd target;
tar -zcvf lib.tar.gz lib/
# for each cluster node:
scp  lib.tar.gz  $clusternode:~/
scp  app.jar  $clusternode:~/

ssh $clusternode
# on the cluster node:
gzip -d lib.tar.gz 
tar -xf lib.tar

```

# How to run


## local mode:

```
 java -Dakka.port=2551 -jar target/app.jar
 java -Dakka.port=2552 -jar target/app.jar 
```


## AWS Cluster mode:
On each node - assuming that there will be min 3 nodes and having following cluster name  

```
java -Dcluster="test-akka-cluster-v2" -DminNodesForQuorumParam=3 -jar app.jar

```

To check if it works :
```
netstat -tnlp |grep 2551

```
